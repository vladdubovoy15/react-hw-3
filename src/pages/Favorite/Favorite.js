import React from 'react';
import Card from "../../components/Card/Card";


const Favorite = ({ favorite, favoriteHandler, cartHandler}) => {

  return (
      <>
        {favorite.length
            ? <ul className={'cards-container'}>
              {favorite.map(card => <Card key={card.id}
                                          card={card}
                                          favorite={favorite}
                                          modalHandler={cartHandler}
                                          favoriteHandler={favoriteHandler}
              />)
              }
            </ul>
            : <h3 className={'red-text center'}>There are no items in favorite list</h3>
        }
      </>
  );
}

export default Favorite;