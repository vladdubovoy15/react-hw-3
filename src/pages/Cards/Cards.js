import React, {useEffect, useState, memo} from 'react';
import axios from 'axios';
import Loader from '../../components/Loader/Loader';
import Card from "../../components/Card/Card";
import './Cards.scss';
import {saveToLocalStorage} from '../../helpers/saveToLocalStorage';

const Cards = ({setCards, cards, cartHandler, favorite, favoriteHandler}) => {
  const [isLoading, setIsLoading] = useState(true);

  useEffect(() => {
    axios.get('/api/phones.json')
        .then((response) => {
          setCards(response.data);
          setIsLoading(false);
          saveToLocalStorage('cards', response.data);
        })
  }, [setCards])

  return (
      <>
        {isLoading
            ? <Loader/>
            : <ul className={'cards-container'}>
              {cards.map(card =>
                  <Card key={card.id}
                        card={card}
                        favorite={favorite}
                        modalHandler={cartHandler}
                        favoriteHandler={favoriteHandler}
                  />)}
            </ul>
        }
      </>
  );
};

export default memo(Cards);