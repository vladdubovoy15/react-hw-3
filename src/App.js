import React, {useCallback, useState} from "react";
import ErrorBoundary from "./components/ErrorBoundary/ErrorBoundary";
import Footer from "./components/Footer/Footer";
import Header from "./components/Header/Header";
import './style/style.scss';
import AppRouter from "./components/AppRouter/AppRouter";
import {getItemFromLocalStorage} from "./helpers/getItemFromLocalStorage";
import {saveToLocalStorage} from "./helpers/saveToLocalStorage";
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/Button";

const App = () => {
  const [isOpenModal, setIsOpenModal] = useState(false);
  const [cart, setCart] = useState(getItemFromLocalStorage('cart') || []);
  const [cards, setCards] = useState(getItemFromLocalStorage('cards') || []);
  const [favorite, setFavorite] = useState(getItemFromLocalStorage('favorite') || []);

  const modalHandler = useCallback(() => {
    setIsOpenModal(!isOpenModal);
  }, [isOpenModal]);

  const cartHandler = useCallback((e, card) => {
    if (e.target.dataset.add) {
      if (!cart.map(e => e.id).includes(card.id)) {
        const cartList = [...cart, card];
        modalHandler()
        setCart(cartList);
      }
    } else {
      const cartList = cart.filter(item => item.id !== card.id);
      modalHandler()
      setCart(cartList);
    }
  }, [cart, modalHandler]);

  const closeModal = useCallback((e) => {
    if (e.target.dataset.cancel) {
      const cart = getItemFromLocalStorage('cart') || [];
      setCart(cart);
    } else {
      saveToLocalStorage('cart', cart);
    }
    modalHandler()
  }, [cart, modalHandler]);

  const favoriteHandler = useCallback((card) => {
    const newCardsList = favorite.map(e => e.id).includes(card.id)
        ? favorite.filter(item => item.id !== card.id)
        : [...favorite, card];
    setFavorite(newCardsList);
    saveToLocalStorage('favorite', newCardsList);
  }, [favorite]);

  return (
      <ErrorBoundary>
        <Header/>
        <main className={"main"}>
          <AppRouter setCards={setCards}
                     cards={cards}
                     cartHandler={cartHandler}
                     favorite={favorite}
                     favoriteHandler={favoriteHandler}
                     cart={cart}
                     isOpenModal={isOpenModal}
                     closeModal={closeModal}

          />
        </main>
        <Footer/>
        {isOpenModal && <Modal modalHandler={closeModal}
                               cancel={true}
                               header={"Do you want to add it to the cart?"}
                               actions={
                                 <>
                                   <Button backgroundColor={"#b3382c"}
                                           text={"Ok"}
                                           clickHandler={closeModal}
                                   />
                                   <Button backgroundColor={"#b3382c"}
                                           text={"Cancel"}
                                           clickHandler={closeModal}
                                           cancel={true}
                                   />
                                 </>
                               }


        />}
      </ErrorBoundary>
  );
}

export default App;
