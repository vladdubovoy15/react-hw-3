import React from 'react';

const Footer = () => (
    <footer className="page-footer">
       <span className="footer-copyright">
          © 2021 Copyright Text
      </span>
    </footer>
);

export default Footer;