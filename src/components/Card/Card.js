import React, {memo} from 'react';
import './Card.scss';
import Icon from "../Icon/Icon";
import PropTypes from 'prop-types';

const Card = ({modalHandler, favoriteHandler, favorite, card, trashCan}) => {
  const {title, price, color, img, code, id} = card;

  return (
      <li className='card z-depth-2'>
        <h3 className='card__title'>{title}</h3>
        <img src={img}
             className={"card__image"}
             alt={title}
             width={200}
             height={240}/>
        <ul>
          <li>code: {code}</li>
          <li>color: {color}</li>
          <li>price: {price}</li>
        </ul>
        <button className={"card__button red waves-effect waves-light"}
                data-add={true}
                onClick={(e) => modalHandler(e, card)}>Add to
          cart
        </button>
        <Icon filled={favorite.map(e => e.id).includes(id)} className={"favorite"}
              favoriteHandler={favoriteHandler} card={card}/>
        {trashCan && <i className="material-icons trash-can"
                        onClick={(e) => modalHandler(e, card)}>delete</i>}
      </li>
  )
};

Card.propTypes = {
  card: PropTypes.object,
  modalHandler: PropTypes.func,
  favoriteHandler: PropTypes.func,
  favorite: PropTypes.arrayOf(PropTypes.object),
  deleteHandler: PropTypes.func,
}

Card.defaultProp = {
  trashCan: false
}

export default memo(Card);