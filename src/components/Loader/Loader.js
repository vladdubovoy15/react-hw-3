import React from 'react';

const Loader = () => (
    <div className="progress red loader">
      <div className="indeterminate white"></div>
    </div>
);

export default Loader;